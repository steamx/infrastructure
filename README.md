# Infrastructure
The launch point for the steamx application. The project contains docker-compose.yaml file which defines the steamx application micro services. It also includes mosquitto configuration file for mosquitto broker which is used for communication between randomizer and notification services.
The last component included in the project is a fake smtp server needed for testing the notification service.

## How to run the steamx app
- Install docker and docker-compose
- Clone all repositories from steamx repo group to the same directory
- Run `docker-compose up` command in the Infrastructure directory

## Roadmap

- [ ] Prepare pipelines for each project with building, testing, deploying steps
- [ ] Create helm charts for deploying apps in the k8s
